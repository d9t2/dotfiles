# shellcheck shell=bash

# Copyright (c) 2024 Dominic Adam Walters

. "$HOME/.config/shell/profile.sh"

# Source .bashrc if running interactive
case "$-" in
    *i*)
        . "$HOME/.bashrc" || echo 1>&2 "ERROR: .bashrc failed with retval $?"
        ;;
    *)
        return
esac
