# shellcheck shell=bash

# Copyright (c) 2022 Dominic Adam Walters

# Config
zstyle :compinstall filename '/home/dawalters/.zshrc'
autoload -Uz compinit
compinit

# History
export HISTFILE="$HOME/.local/zsh/histfile"
export HISTSIZE=10000
export SAVEHIST=$HISTSIZE

# Common options
setopt extendedglob nomatch notify
unsetopt autocd beep

# Emacs keys
bindkey -e

# Fix the delete key
bindkey "^[[3~" delete-char

# Theme
if [ -f "$HOME/.config/zsh/headline/headline.zsh-theme" ]; then

    . "$HOME/.config/zsh/headline/headline.zsh-theme"
    ## Visuals
    # shellcheck disable=SC2154
    export HEADLINE_STYLE_DEFAULT="$black_back"
    export HEADLINE_LINE_MODE="on"
    ## User
    export HEADLINE_USER_PREFIX="  "
    ## Host
    export HEADLINE_DO_HOST="false"
    export HEADLINE_HOST_TO_PATH=" in "
    ## Path
    export HEADLINE_PATH_PREFIX="󰉋 "
    ## Git: Branch
    export HEADLINE_PAD_TO_BRANCH=" on "
    export HEADLINE_BRANCH_PREFIX=" "
    ## Git: Status
    export HEADLINE_DO_GIT_STATUS_COUNTS="true"
    export HEADLINE_DO_GIT_STATUS_OMIT_ONE="true"
    export HEADLINE_BRANCH_TO_STATUS=" ["
    export HEADLINE_GIT_STAGED=""
    export HEADLINE_GIT_CHANGED="~"
    export HEADLINE_GIT_UNTRACKED=""
    export HEADLINE_GIT_BEHIND=""
    export HEADLINE_GIT_AHEAD=""
    export HEADLINE_GIT_DIVERGED=""
    export HEADLINE_GIT_STASHED=""
    export HEADLINE_GIT_CONFLICTS=""
    export HEADLINE_STATUS_TO_STATUS="|"
    export HEADLINE_STATUS_END="] "
    ## Prompt
    export HEADLINE_PROMPT=" "
    ## Exit code
    export HEADLINE_DO_ERR=true
    export HEADLINE_ERR_PREFIX=" "

fi

# Ring the bell
function precmd() {
    echo -n -e "\a"
}

# Syntax highlighting plugin
prefixes=(\
    "/usr/local/share" \
    "/usr/share" \
    "/usr/share/zsh/plugins"\
)
for prefix in "${prefixes[@]}"; do
    if [ -d "$prefix/zsh-syntax-highlighting" ]; then
        . "$prefix/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
    fi
done

# Autosuggestions highlighting plugin
prefixes=(\
    "/usr/share" \
    "/usr/share/zsh/plugins"\
)
for prefix in "${prefixes[@]}"; do
    if [ -d "$prefix/zsh-autosuggestions" ]; then
        . "$prefix/zsh-autosuggestions/zsh-autosuggestions.zsh"
        bindkey "^l" autosuggest-execute
    fi
done

# Common shell stuff
. "$HOME/.config/shell/rc.sh"
