# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# If not running interactively, don't do anything
case "$-" in
    *i*)
        ;;
    *)
        return
esac

# GPG Setup
this_tty="$(tty)"
rc="$?"
if [ "$rc" -eq 0 ]; then
    export GPG_TTY="$this_tty"
fi

# Source the rc.sh files
find "$HOME/.config/shell" -name "rc.sh" -not -path "$HOME/.config/shell/rc.sh" | \
    while IFS= read -r file; do
        # shellcheck disable=SC1090
        . "$file"
    done
find "$HOME/.config/shell/aliases" -name "*.sh" | \
    while IFS= read -r file; do
        # shellcheck disable=SC1090
        . "$file"
    done

# XOrg
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
    exec startx
fi
