# shellcheck shell=sh

# Copyright (c) 2024 Dominic Adam Walters

# Sync vale if necessary
if command -v vale > /dev/null 2>&1; then

    make -s -C "$HOME/.config/shell/programs/vale"

fi
