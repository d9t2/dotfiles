# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# If eza is available, use it instead of ls
if command -v eza > /dev/null 2>&1; then

    eza_version="$(eza --version | head -n 2 | tail -n 1 | cut -d" " -f 1 | cut -c2-)"
    eza_major_version="$(echo "$eza_version" | cut -d"." -f1)"
    eza_minor_version="$(echo "$eza_version" | cut -d"." -f2)"

    if [ "$eza_major_version" -eq 0 ] && [ "$eza_minor_version" -le 15 ]; then
        eza_icons_flag="--icons"
    else
        eza_icons_flag="--icons=auto"
    fi
    eza_common_flags="--color=always --group-directories-first $eza_icons_flag"

    # shellcheck disable=SC2139
    alias ls="eza      $eza_common_flags"
    # shellcheck disable=SC2139
    alias la="eza -a   $eza_common_flags"
    # shellcheck disable=SC2139
    alias ll="eza -l   $eza_common_flags"
    # shellcheck disable=SC2139
    alias lt="eza -aT  $eza_common_flags"
    # shellcheck disable=SC2139
    alias l.="eza -a   $eza_icons_flag | grep -E '^\.'"

    # Alias exa to eza
    alias exa="eza"

fi
