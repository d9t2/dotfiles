# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# If `doas` is available, use it instead of `sudo`
if command -v doas > /dev/null 2>&1; then

    alias sudo="doas"

    doasedit() {
        doas -u "$USER" "$EDITOR" "$@"
    }

    alias sudoedit="doasedit"

fi
