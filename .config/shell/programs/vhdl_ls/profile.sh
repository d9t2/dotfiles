# shellcheck shell=sh

# Copyright (c) 2024 Dominic Adam Walters

if command -v vhdl_ls > /dev/null 2>&1; then

    libraries_name=vhdl_libraries
    libraries_dir="$HOME/.cargo/share/$libraries_name"
    libraries_parent="$(dirname "$libraries_dir")"

    if [ ! -d "$libraries_dir" ]; then

        mkdir -p "$libraries_parent"

        vhdl_ls_version="$(vhdl_ls --version | cut -d" " -f2)"
        clone_path=/tmp/rust_hdl
        echo "Setting up \`vhdl_ls\`..."
        git clone https://github.com/VHDL-LS/rust_hdl.git \
            --quiet \
            --single-branch \
            --depth 1 \
            --branch v"$vhdl_ls_version" \
            "$clone_path" > "$clone_path".log 2>&1
        rc=$?
        if [ $rc -ne 0 ]; then
            echo "\`git clone\` failed for \`vhdl_ls\`"
            cat "$clone_path".log
            rm -rf "$clone_path"*
            return $rc
        fi

        cp -rf "$clone_path"/"$libraries_name" "$libraries_parent"/.
        rm -rf "$clone_path"*

    fi

fi
