# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Better dev tools on RHEL
if [ -f /opt/rh/devtoolset-8/enable ]; then
    . /opt/rh/devtoolset-8/enable
fi
