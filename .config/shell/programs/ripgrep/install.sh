# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

if ! command -v rg > /dev/null 2>&1; then
    cargo_install ripgrep ;
fi
