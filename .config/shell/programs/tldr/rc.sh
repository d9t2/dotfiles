# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Colours
export TLDR_COLOR_NAME="red bold"
export TLDR_COLOR_DESCRIPTION="white"
export TLDR_COLOR_EXAMPLE="blue bold"
export TLDR_COLOR_COMMAND="magenta"
export TLDR_COLOR_PARAMETER="white"
# Language
export TLDR_LANGUAGE="en"
# Cache
export TLDR_CACHE_ENABLED=1
export TLDR_CACHE_MAX_AGE=720  # hours
# When behind a proxy:
# export TLDR_ALLOW_INSECURE=1
# Remote sources
export TLDR_PAGES_SOURCE_LOCATION="https://raw.githubusercontent.com/tldr-pages/tldr/main/pages"
export TLDR_DOWNLOAD_CACHE_LOCATION="https://tldr-pages.github.io/assets/tldr.zip"
