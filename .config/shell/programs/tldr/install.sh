# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

if ! command -v tldr > /dev/null 2>&1; then
    cargo_install tldr ;
fi
