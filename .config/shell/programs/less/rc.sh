# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Better `less`
export LESS="-R -F"
less_version=$(less --version | grep -E "^less" | head -n 1 | cut -d" " -f 2)
if [ "$less_version" -gt 539 ]; then
    export LESS="$LESS --mouse"
fi
if [ "$less_version" -gt 542 ]; then
    export LESS="$LESS --wheel-lines=3"
fi
export LESSEDIT="%E %f"
