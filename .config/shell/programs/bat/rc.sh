# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

if command -v bat > /dev/null 2>&1; then

    # Use crappy bat as cat
    alias cat="bat --paging=never --plain"

    # Colourise `man` with `bat`
    export MANPAGER="sh -c 'col -bx | bat -l man -p'"
    export MANROFFOPT="-c"

    # `watch` with syntax highlighting
    batwatch() {
        # Demand a file name
        if [ -z "$1" ]; then
            echo "Usage: batwatch <file> [<lines> [<language>]]"
        fi
        # Use line count if supplied
        if [ -z "$2" ]; then
            lines=""
        else
            lines="-n $2"
        fi
        # Use language if supplied
        if [ -z "$3" ]; then
            language=""
        else
            language="-l $3"
        fi
        # Run command
        tail -f "$1" "$lines" | bat --style=plain --paging=never "$language"
    }

    # Colourise any page that implements `--help`
    help() {
        "$@" --help 2>&1 | bat --plain --language=help
    }

    # Build cache if not built
    if [ ! -f "$HOME/.cache/bat/metadata.yaml" ]; then
        bat cache --build
    fi

fi
