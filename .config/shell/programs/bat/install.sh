# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

if ! command -v bat > /dev/null 2>&1; then
    case "$ID" in
        "arch")
            pacman_install bat ;;
        "ubuntu")
            cargo_install bat ;;
        *)
            echo "Can't install bat for '$ID': OS not recognised"
    esac
fi
