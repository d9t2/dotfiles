# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

if ! command -v curl > /dev/null 2>&1; then
    case "$ID" in
        "arch")
            pacman_install curl ;;
        "ubuntu")
            apt_install curl ;;
        *)
            echo "Can't install curl for '$ID': OS not recognised"
    esac
fi
