# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# If `nvim` is available, use it instead of `vi`, `vim`, and `nano`
if command -v nvim > /dev/null 2>&1; then

    export EDITOR=nvim

    alias vi="nvim"
    alias vim="nvim"
    alias nano="nvim"

fi
