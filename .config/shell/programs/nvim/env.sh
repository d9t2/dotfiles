# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Add programs installed via mason to path
case ":${PATH}:" in
    *:"$HOME/.local/share/nvim/mason/bin":*)
        ;;
    *)
        export PATH="$HOME/.local/share/nvim/mason/bin:$PATH"
        ;;
esac
