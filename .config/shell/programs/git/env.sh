# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Add expanded git commands to PATH
case ":${PATH}:" in
    *:"$HOME/.config/shell/programs/git/commands":*)
        ;;
    *)
        export PATH="$HOME/.config/shell/programs/git/commands:$PATH"
        ;;
esac
