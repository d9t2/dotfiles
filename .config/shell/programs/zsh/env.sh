# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Make sure `histfile` can be made
mkdir -p "$HOME/.local/zsh"
