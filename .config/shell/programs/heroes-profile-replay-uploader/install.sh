# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

if ! command -v heroes-profile-replay-uploader > /dev/null 2>&1; then
    uv tool install heroes-profile-replay-uploader
fi
