# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Configure cargo
if [ -f "$HOME/.cargo/env" ]; then
    . "$HOME/.cargo/env"
fi
