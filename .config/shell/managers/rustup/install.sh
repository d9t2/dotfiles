# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

if ! command -v rustup > /dev/null 2>&1; then
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
fi
