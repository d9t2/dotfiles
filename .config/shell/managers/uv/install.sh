# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

if ! command -v uv > /dev/null 2>&1; then
    curl -LsSf https://astral.sh/uv/install.sh | sh
fi
