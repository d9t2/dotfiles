# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

uv=$(command -v uv || command -v "$HOME"/local/bin/uv)

if [ -n "$uv" ]; then
    python_dir=$(uv python dir)

    # Make links directory and add to path
    links_dir="$python_dir/links"
    mkdir -p "$links_dir"
    case ":${PATH}:" in
        *:"$links_dir":*)
            ;;
        *)
            export PATH="$links_dir:$PATH"
            ;;
    esac

    # Add a symlink for each python version to the links folder
    for bin_dir in "$python_dir"/*/bin; do
        version=$(echo "$bin_dir" | cut -d "-" -f 2 | cut -d "." -f 1-2)
        if [ -f "$bin_dir/pip$version" ]; then
            ln -frs "$bin_dir/pip$version" "$links_dir/pip$version"
        fi
        if [ -f "$bin_dir/pydoc$version" ]; then
            ln -frs "$bin_dir/pydoc$version" "$links_dir/pydoc$version"
        fi
        if [ -f "$bin_dir/python$version" ]; then
            ln -frs "$bin_dir/python$version" "$links_dir/python$version"
        fi
        if [ -f "$bin_dir/python$version-config" ]; then
            ln -frs "$bin_dir/python$version-config" "$links_dir/python$version-config"
        fi
    done
fi
