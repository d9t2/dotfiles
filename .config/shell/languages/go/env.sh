# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Move `GOPATH` to `.local`
export GOPATH="$HOME/.local/go"

# Add a common `go` install directory to path if it exists
if [ -d /usr/local/go/bin ]; then
    case ":${PATH}:" in
        *:"/usr/local/go/bin":*)
            ;;
        *)
            export PATH="/usr/local/go/bin:$PATH"
            ;;
    esac
fi

# Add `GOPATH/bin` to `PATH`
case ":${PATH}:" in
    *:"$GOPATH/bin":*)
        ;;
    *)
        export PATH="$GOPATH/bin:$PATH"
        ;;
esac
