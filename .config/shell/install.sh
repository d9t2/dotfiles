# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

# Formatting functions
dotfiles_colour_red="\e[31m"
dotfiles_colour_green="\e[32m"
dotfiles_colour_end="\e[0m"

dotfiles_format_good() {
    printf "%s" "$dotfiles_colour_green$1$dotfiles_colour_end"
}

dotfiles_format_bad() {
    printf "%s" "$dotfiles_colour_red$1$dotfiles_colour_end"
}

dotfiles_declare_installation() {
    printf "\n"
    printf "%s\n" "$(dotfiles_format_good "Installing \`$2\` with \`$1\`...")"
}

# Sudo binary
dotfiles_sudo=$(command -v doas || command -v sudo)

# Functions to run package manager installers
apt_install() {
    dotfiles_declare_installation "apt-get" "$@"
    "$dotfiles_sudo" apt-get install -y "$@"
    ret="$?"
    if [ "$ret" -ne 0 ]; then
        printf "%s\n" "$(dotfiles_format_bad "Failed to install \`$*\` with \`apt-get\`...")"
    fi
}

cargo_install() {
    dotfiles_declare_installation "cargo" "$@"
    cargo install "$@"
    ret="$?"
    if [ "$ret" -ne 0 ]; then
        printf "%s\n" "$(dotfiles_format_bad "Failed to install \`$*\` with \`cargo\`...")"
    fi
}

flatpak_install() {
    dotfiles_declare_installation "flatpak" "$@"
    flatpak install "$@"
    ret="$?"
    if [ "$ret" -ne 0 ]; then
        printf "%s\n" "$(dotfiles_format_bad "Failed to install \`$*\` with \`flatpak\`...")"
    fi
}

pacman_install() {
    dotfiles_declare_installation "pacman" "$@"
    "$dotfiles_sudo" pacman -Sy "$@"
    ret="$?"
    if [ "$ret" -ne 0 ]; then
        printf "%s\n" "$(dotfiles_format_bad "Failed to install \`$*\` with \`pacman\`...")"
    fi
}

# Get ID and VERSION_ID for OS
if [ -f /etc/os-release ]; then
    . /etc/os-release
else
    echo "/etc/os-release not found"
fi

##############
# Installers #
##############
dotfiles_activate_installs() {
    sed -i \
        "s/DOTFILES_SKIP_INSTALLS=\".*\"/DOTFILES_SKIP_INSTALLS=\"\"/g" \
        "$HOME/.config/shell/install.sh"
}
dotfiles_deactivate_installs() {
    sed -i \
        "s/DOTFILES_SKIP_INSTALLS=\"\"/DOTFILES_SKIP_INSTALLS=\"1\"/g" \
        "$HOME/.config/shell/install.sh"
}

DOTFILES_SKIP_INSTALLS=""
if [ -z "$DOTFILES_SKIP_INSTALLS" ]; then

    . "$HOME/.config/shell/programs/curl/install.sh"

    # Needs `curl`
    . "$HOME/.config/shell/managers/rustup/install.sh"
    . "$HOME/.config/shell/managers/uv/install.sh"

    # Needs `uv`
    . "$HOME/.config/shell/programs/heroes-profile-replay-uploader/install.sh"

    # Needs `cargo` (installed with `rustup`)
    . "$HOME/.config/shell/programs/bat/install.sh"
    . "$HOME/.config/shell/programs/ripgrep/install.sh"
    . "$HOME/.config/shell/programs/tldr/install.sh"

fi
