# shellcheck shell=sh

# Copyright (c) 2024 Dominic Adam Walters

# Source the profile.sh files
find "$HOME/.config/shell" -name "profile.sh" -not -path "$HOME/.config/shell/profile.sh" | \
    while IFS= read -r file; do
        # shellcheck disable=SC1090
        . "$file"
    done
