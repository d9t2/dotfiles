# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

# Print the list of largest files in a given directory
largest_files() {
    if [ $# -eq 1 ]; then
        dir=$1
    else
        dir=.
    fi
    find "$dir" -type f -exec du -h {} + | sort -h
}
