# shellcheck shell=bash

keybinds() {
    command_set="$*"
    shell=(
        "# General Shell"
        "# # Navigation"
        " - Ctrl-p:   Up"
        " - Ctrl-n:   Down"
        " - Ctrl-b:   Left"
        " - Ctrl-f:   Right"
        " - Ctrl-a:   Home / Move to start of line"
        " - Ctrl-e:   End / Move to end of line"
        " - Ctrl-r:   Search past commands"
        " - Ctrl-g:   Leave history search"
        " - Alt-b:    Move back one word"
        " - Alt-f:    Move forward one word"
        "# # Commands"
        " - Ctrl-o:   Execute current line"
        " - Ctrl-l:   Clear screen"
        " - Ctrl-c:   SIGINT"
        " - Ctrl-d:   EOF"
        " - Ctrl-x+e: Open \$EDITOR with current command"
        " - Ctrl-s:   Stop output to terminal"
        " - Ctrl-q:   Start output to terminal"
        " - Ctrl-z:   SIGSTP / Use \`fg\` to start again"
        "# # Manipulate Current Command"
        " - Ctrl-d:   Delete under cursor"
        " - Ctrl-h:   Delete before cursor"
        " - Ctrl-k:   Delete from cursor to end of line"
        " - Ctrl-w:   Delete last word"
        " - Ctrl-y:   Paste last deleted item"
        " - Ctrl-t:   Switch the last two characters"
        " - Alt-c:    Capitalise current character"
        " - Alt-u:    Uppercase to end of word"
        " - Alt-l:    Lowercase to end of word"
        " - Alt-t:    Switch the last two words"
        " - Alt-.:    Paste last word of previous command"
        " "
        "# Bash Specific"
        " - Ctrl-u: Delete from cursor to start of line"
        " "
        "# ZSH Specific"
        " - Ctrl-u:   Delete line"
        " - Ctrl-x+v: Vi Mode"
    )
    alacritty=(
        "# Alacritty"
        " - Ctrl--:           Decrease font size"
        " - Ctrl-=:           Increase font size"
        " - Ctrl-Shift-Space: Alacritty Vi Mode"
        " - Ctrl-Shift-f:     Search Forwards"
        " - Ctrl-Shift-b:     Search Backwards"
    )
    vim=(
        "# Vim"
        "# # Command Motion Format"
        " - <command><number><motion>: Do <command> on <motion> <number> times"
        " "
        "# # Commands"
        " - d:       Delete"
        " - u:       Undo"
        " - y / Y:   Yank"
        " - p:       Paste"
        " - >:       Indent"
        " - Ctrl-a:  Increment all numbers under cursor"
        " - gCtrl-a: Successively increment all numbers under the cursor"
        " - Ctrl-x:  Decrement all numbers under cursor"
        " - gCtrl-x: Successively decrement all numbers under the cursor"
        " "
        "# # Modes"
        " - i:       Enter insert mode on left side of cursor"
        " - a:       Enter insert mode on right side of cursor"
        " - I:       Enter insert mode at start of line"
        " - A:       Enter insert mode at end of line"
        " - o:       New line and enter insert mode"
        " - O:       New line above current line and enter insert mode"
        " - v:       Enter visual mode"
        " - Ctrl-v:  Enter visual block mode"
        " - Shift-v: Enter visual line mode"
        " "
        "# # Motions: Normal Mode"
        " - j:       Down"
        " - k:       Up"
        " - h:       Left"
        " - l:       Right"
        " - w:       Move right one word"
        " - b:       Move left right word"
        " - ^:       Move to start of first word"
        " - _:       Move to start of first word"
        " - $:       Move to end of line"
        " - 0:       Move to start of line"
        " - f<char>: Move to next occurrence of <char>"
        " - F<char>: Move to prev occurrence of <char>"
        " - t<char>: Move to before next occurrence of <char>"
        " - T<char>: Move to after prev occurrence of <char>"
        " - ;:       Move to next match for current motion"
        " - ,:       Move to previous match for current motion"
        " - i<char>: Execute command on everything between <char> instances"
        " - a<char>: Execute command on everything between and including <char> instances"
        " - <num>G:  Move to line <num>"
        " - G:       Move to last line"
        " - gg:      Move to first line"
        " - Ctrl-d:  Move half a page down"
        " - Ctrl-u:  Move half a page up"
        " - {:       Move up one paragraph"
        " - }:       Move down one paragraph"
        " - %:       Move to matching bracket"
        " "
        "# # Motions: Visual Mode"
        " - o: Switch cursor to other side of visual highlight"
        " "
        "# # Search"
        " - /:       Search down"
        " - ?:       Search up"
        " - *:       Search down for word under cursor"
        " - #:       Search up for word under cursor"
        " - n:       Go to next search result"
        " - N:       Go to previous search result"
        " "
        "# # View"
        " - zz: Center on the cursor"
    )
    output=()
    if [ -z "$command_set" ]; then
        output+=(\
            "${shell[@]}" \
            " " \
            "${vim[@]}" \
            " " \
            "${alacritty[@]}" \
        )
    fi
    for com in $command_set; do
        if [ "$com" = "shell" ]; then
            output+=("${shell[@]}")
        elif [ "$com" = "vim" ]; then
            output+=("${vim[@]}")
        elif [ "$com" = "alacritty" ]; then
            output+=("${alacritty[@]}")
        fi
        output+=(" ")
    done
    printf '%s\n' "${output[@]}"
}
