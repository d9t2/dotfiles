# shellcheck shell=bash

# Copyright (c) 2024 Dominic Adam Walters

if ! command -v missioncenter &> /dev/null; then
    if command -v flatpak &> /dev/null; then
        if flatpak list | grep "Mission Center" &> /dev/null; then

            alias missioncenter='flatpak run $(flatpak list | grep "Mission Center" | tr -s " " | cut -f2) &> /dev/null &'

        fi
    fi
fi
