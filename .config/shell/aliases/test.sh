# shellcheck shell=sh

# Copyright (c) 2025 Dominic Adam Walters

# Display a selection of different underlines to terminal
# https://github.com/alacritty/alacritty/issues/6704#issuecomment-1431907735
test_underlining() {
    printf "\e[58;2;255;0;255m\e[4:1mUNDERLINE\e[4:2mDOUBLE\e[58:5:196m\e[4:3mUh̷̗ERCURL\e[4:4mDOTTED\e[4:5mDASHED\e[59mNOT_COLORED_DASH\e[0m\n"
    printf "\x1b[58:2::255:0:0m\x1b[4:1msingle\x1b[4:2mdouble\x1b[4:3mcurly\x1b[4:4mdotted\x1b[4:5mdashed\x1b[0m\n"
}
