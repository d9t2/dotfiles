# shellcheck shell=sh

# Copyright (c) 2022-2023 Dominic Adam Walters

# Ping all addresses on a subnet.
# This can be done concurrently at the cost of determinism, or chronologically.
ping_subnet() {
    # Arguments
    if [ $# -lt 1 ] || [ $# -gt 1 ]; then
        printf '%s\n' \
            "Usage:" \
            "  ping_subnet <partial-ip>" \
            "Example:" \
            "  ping_subnet 192.168.7"
        return 1
    fi
    partial_ip=$1
    # Iterate through IPs ending 1 to 254
    i=1
    while [ "$i" -le 255 ]; do
        (ping -c 1 "$partial_ip.$i" 2>&1 | grep "bytes from" &)
        i=$((i + 1))
    done
}

# Mount an nfs share to a directory
mount_nfs_share() {
    # Arguments
    if [ $# -ne 3 ]; then
        printf '%s\n' \
            "Usage:" \
            "  mount_nfs_share <ip> <from-dir> <to-dir>" \
            "Example:" \
            "  mount_nfs_share 192.168.0.122 /srv/nfs/my_dir /mnt"
        exit 1
    fi
    ip=$1
    from_dir=$2
    to_dir=$3
    # Action
    sudo mount -t nfs "$ip:$from_dir" "$to_dir"
}
