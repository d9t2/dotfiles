# shellcheck shell=bash

# Copyright (c) 2022-2023 Dominic Adam Walters

# A script to update all installed software on the system.

os_update_all() {

    # Formatting functions
    colour_red="\e[31m"
    colour_green="\e[32m"
    colour_end="\e[0m"

    title() {
        echo ""
        echo -e "$(format_good "Updating programs installed via \`$1\`...")"
    }

    format_good() {
        echo -e "$colour_green$1$colour_end"
    }

    format_bad() {
        echo -e "$colour_red$1$colour_end"
    }

    success() {
        echo -e "$(format_good "Successfully ran \`$1\`.")"
    }

    failure() {
        echo -e "$(format_bad "Bad return code when running \`$1\`: $2.")"
    }

    message() {
        if [ "$2" -ne 0 ]; then
            failure "$1" "$2"
        else
            success "$1"
        fi
    }

    run_command() {
        eval "$1"
        ret_code=$?
        message "$1" $ret_code
        return $ret_code
    }

    # Arch Linux: aur
    if command -v aur &> /dev/null; then
        title "aur"
        run_command "aur sync -u" || return $?
        run_command "sudo aur repo -d builds --list | aur vercmp" || return $?
    fi

    # nvim
    if command -v nvim &> /dev/null; then
        title "nvim"
        run_command "nvim --headless \"+Lazy! sync +TSUpdate +MasonUpdateAll\" +qa" || return $?
    fi

    # Package Managers
    # # apt
    if command -v apt &> /dev/null; then
        title "apt"
        run_command "sudo apt -y update" || return $?
        run_command "sudo apt -y upgrade" || return $?
    fi

    # # pacman
    if command -v pacman &> /dev/null; then
        title "pacman"
        run_command "sudo pacman -Syu --noconfirm" || return $?
    fi

    # # yum
    if command -v yum &> /dev/null; then
        title "yum"
        run_command "sudo yum -y update" || return $?
    fi

    # pipx
    if command -v pipx &> /dev/null; then
        title "pipx"
        run_command "pipx upgrade-all" || return $?
    fi

    # uv
    if command -v uv &> /dev/null; then
        if [ "$(command -v uv)" != "/usr/bin/uv" ]; then
            title "uv"
            run_command "uv self update" || return $?
        fi
    fi

    # rust
    # # rustup
    if command -v rustup &> /dev/null; then
        title "rustup"
        run_command "rustup update" || return $?
    fi

    # # cargo
    if command -v cargo &> /dev/null; then
        title "cargo"
        for package in $(\
            cargo install --list \
                | grep -E '^[a-z0-9_-]+ v[0-9.]+:$' \
                | cut -f1 -d' ' \
        ); do
            run_command "cargo install $package" || return $?
        done
    fi

    # snap
    if command -v snap &> /dev/null; then
        title "snap"
        run_command "sudo snap refresh" || return $?
    fi

    # flatpak
    if command -v flatpak &> /dev/null; then
        title "flatpak"
        run_command "flatpak update -y" || return $?
    fi

}
