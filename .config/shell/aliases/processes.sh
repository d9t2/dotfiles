# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Displays processes and how many X clients they are using. Useful if you've
# hit the process limit and need to close something.
processcount() {
    lsof -U +c 15 | cut -f1 -d' ' | sort | uniq -c | sort -rn
}
