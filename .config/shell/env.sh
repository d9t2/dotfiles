# shellcheck shell=sh

# Copyright (c) 2023 Dominic Adam Walters

# Add local binaries to PATH (e.g. pip, pipx)
case ":${PATH}:" in
    *:"$HOME/.local/bin":*)
        ;;
    *)
        export PATH="$HOME/.local/bin:$PATH"
        ;;
esac

# Source the env.sh files
find "$HOME/.config/shell" -name "env.sh" -not -path "$HOME/.config/shell/env.sh" | \
    while IFS= read -r file; do
        # shellcheck disable=SC1090
        . "$file"
    done

# Perform any required installs
. "$HOME/.config/shell/install.sh"
