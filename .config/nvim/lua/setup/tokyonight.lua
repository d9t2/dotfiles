-- Configure
require("tokyonight").setup({
    style = "storm",
    sidebars = { "packer" },  -- Darker background
    dim_inactive = true,
})

-- Apply
vim.api.nvim_cmd({cmd = "colorscheme", args = {"tokyonight"}}, {})
