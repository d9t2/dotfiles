require("tiny-inline-diagnostic").setup({
    preset = "powerline",
    options = {
        format = function(diagnostic)
            if diagnostic.source == nil or diagnostic.source == "" then
                return diagnostic.message
            elseif diagnostic.code == nil or diagnostic.code == "" then
                return diagnostic.message .. " (" .. diagnostic.source .. ")"
            else
                return diagnostic.message .. " (" .. diagnostic.source .. ", " .. diagnostic.code .. ")"
            end
        end,
        set_arrow_to_diag_color = false,
    }
})
vim.diagnostic.config({ virtual_text = false })
