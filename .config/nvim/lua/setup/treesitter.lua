-- Setup custom vhdl treesitter

local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
parser_config.vhdl = {
    install_info = {
        url = "https://github.com/jpt13653903/tree-sitter-vhdl.git",
        files = { "src/parser.c", "src/scanner.c" },
        branch = "develop",
        generate_requires_npm = false,
        requires_generate_from_grammar = false,
    },
    filetype = "vhd",
}

-- Actual setup
require("nvim-treesitter.configs").setup {
    -- One of "all", "maintained" (parsers with maintainers), or a list of languages
    ensure_installed = {
        "bash",
        "bitbake",
        "c",
        "cmake",
        "comment",
        "cpp",
        "devicetree",
        "diff",
        "dockerfile",
        "doxygen",
        "git_config",
        "git_rebase",
        "gitattributes",
        "gitcommit",
        "gitignore",
        "json",
        "kconfig",
        "latex",
        "lua",
        "luadoc",
        "luap",
        "make",
        "markdown",
        "markdown_inline",
        "mermaid",
        "nix",
        "objdump",
        "printf",
        "python",
        "regex",
        "requirements",
        "regex",
        "rust",
        "tcl",
        "tmux",
        "toml",
        "verilog",
        "vhdl",
        "vim",
        "vimdoc",
        "xml",
        "yaml",
    },

    -- Install languages synchronously (only applied to `ensure_installed`)
    sync_install = false,

    -- List of parsers to ignore installing
    ignore_install = {},

    highlight = {
        -- `false` will disable the whole extension
        enable = true,

        -- list of language that will be disabled
        disable = {},

        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
    },
}

-- Interpret Xilinx Design Constraints as TCL
vim.filetype.add({
    pattern = {
        [".*xdc"] = "xdc",
    },
})
vim.treesitter.language.register("tcl", "xdc")

-- Interpret vifm config files as vimscript
vim.filetype.add({
    pattern = {
        [".*vifmrc"] = "vifm",
    },
})
vim.treesitter.language.register("vim", "vifm")

-- Interpret requirements*.txt as requirements
vim.filetype.add({
    pattern = {
        ["requirements.*.txt"] = "requirements",
    },
})
