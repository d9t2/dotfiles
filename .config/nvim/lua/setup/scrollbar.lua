-- Apply tokyonight colours
local colors = require("tokyonight.colors").setup(require("tokyonight.config"))
require("scrollbar").setup({
    handle = {
        color = colors.bg_highlight,
    },
    handlers = {
        gitsigns = true,
    },
    marks = {
        Search = { color = colors.orange },
        Error = { color = colors.error },
        Warn = { color = colors.warning },
        Info = { color = colors.info },
        Hint = { color = colors.hint },
        Misc = { color = colors.purple },
        GitAdd = { text = "" },
        GitChange = { text = "󰜥" },
        GitDelete = { text = "" },
    }
})
