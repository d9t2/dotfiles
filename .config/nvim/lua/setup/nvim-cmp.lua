local has_words_before = function()
    unpack = unpack or table.unpack
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local lspkind = require("lspkind")
local luasnip = require("luasnip")
local cmp = require("cmp")

cmp.setup({
    -- Options to render in explicit window (slightly laggy)
    window = {
        completion = {
            winhighlight = "Normal:Pmenu,FloatBorder:Pmenu,Search:None",
            col_offset = 1,
            side_padding = 0,
        },
    },
    -- Formatting
    formatting = {
        fields = { "kind", "abbr", "menu" },
        format = function(entry, vim_item)
            local kind = lspkind.cmp_format({
                mode = "symbol_text",
                menu = ({
                    buffer = "[Buffer]",
                    calc = "[Maths]",
                    cmdline = "[Command]",
                    conventionalcommits = "[Commits]",
                    dotenv = "[ENV]",
                    emoji = "[Emoji]",
                    git = "[Git]",
                    luasnip = "[LuaSnip]",
                    nerdfont = "[Nerd]",
                    nvim_lsp = "[LSP]",
                    nvim_lua = "[Lua]",
                    path = "[Path]",
                    pypi = "[PyPi]",
                }),
                maxwidth = 50,
                ellipsis_char = '...',
                show_labelDetails = true, -- show labelDetails in menu. Disabled by default
                -- The function below will be called before any actual modifications from lspkind
                -- so that you can provide more controls on popup customization. (See [#30](https://github.com/onsails/lspkind-nvim/pull/30))
                before = function (_before_entry, before_vim_item)
                    -- ...
                    return before_vim_item
                end
            })(entry, vim_item)
            if entry.source.name == "calc" then
                vim_item.kind = "󰃬"
            end
            local strings = vim.split(kind.kind, "%s", { trimempty = true })
            kind.kind = " " .. (strings[1] or "") .. " "
            kind.menu = strings[2] or ""
            return kind
        end,
    },
    -- Snippet engine
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    -- Key maps
    mapping = {
        -- # Scroll down completions
        ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            -- You could replace the expand_or_jumpable() calls with
            -- expand_or_locally_jumpable(), that way you will only jump inside the
            -- snippet region
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            elseif has_words_before() then
                cmp.complete()
            else
                fallback()
            end
        end, { "i", "s" }),
        -- # Scroll up completions
        ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback()
            end
        end, { "i", "s" }),
        -- # Return to confirm, but only if explicitly selecting
        ["<CR>"] = cmp.mapping.confirm({ select = false }),
    },
    sources = cmp.config.sources({
        { name = "nvim_lsp" },
        { name = "luasnip" },
        { name = "git" },
        { name = "pypi", keyword_length = 4 },
        { name = "calc" },
        { name = "path" },
        { name = "emoji", option = { insert = true } },
        { name = "nerdfont" },
        { name = "nvim_lua" },
        { name = "dotenv" },
    }, {
        { name = "buffer" },
    }),
})

-- Set configuration for specific filetype.
cmp.setup.filetype("gitcommit", {
    sources = cmp.config.sources({
        { name = "git" },
        { name = "conventionalcommits" },
    }, {
        { name = "buffer" },
    }),
})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline({ "/", "?" }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
        { name = "buffer" },
    },
})

-- Use cmdline & path source for ":" (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(":", {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        { name = "path" },
    }, {
        { name = "cmdline" },
    }),
    matching = { disallow_symbol_nonprefix_matching = false },
})

-- TODO: is there a better way of doing this?
local colours = require("tokyonight.colors").setup({
    style = "storm",
})

-- Customization for Pmenu
vim.api.nvim_set_hl(0, "Pmenu", { fg = colours.comment, bg = colours.bg_dark })
-- vim.api.nvim_set_hl(0, "PmenuSel", { fg = "NONE" , bg = colours.blue })

vim.api.nvim_set_hl(0, "CmpItemKindClass", { fg = colours.bg_dark, bg = colours.blue1 })
vim.api.nvim_set_hl(0, "CmpItemKindColor", { fg = colours.bg_dark, bg = colours.red })
vim.api.nvim_set_hl(0, "CmpItemKindConstant", { fg = colours.bg_dark, bg = colours.orange })
vim.api.nvim_set_hl(0, "CmpItemKindConstructor", { fg = colours.bg_dark, bg = colours.blue })
vim.api.nvim_set_hl(0, "CmpItemKindEnum", { fg = colours.bg_dark, bg = colours.blue })
vim.api.nvim_set_hl(0, "CmpItemKindEnumMember", { fg = colours.bg_dark, bg = colours.orange })
vim.api.nvim_set_hl(0, "CmpItemKindEvent", { fg = colours.bg_dark, bg = colours.red })
vim.api.nvim_set_hl(0, "CmpItemKindField", { fg = colours.bg_dark, bg = colours.green })
vim.api.nvim_set_hl(0, "CmpItemKindFile", { fg = colours.bg_dark, bg = colours.blue0 })
vim.api.nvim_set_hl(0, "CmpItemKindFolder", { fg = colours.bg_dark, bg = colours.blue0 })
vim.api.nvim_set_hl(0, "CmpItemKindFunction", { fg = colours.bg_dark, bg = colours.blue })
vim.api.nvim_set_hl(0, "CmpItemKindInterface", { fg = colours.bg_dark, bg = colours.red })
vim.api.nvim_set_hl(0, "CmpItemKindKeyword", { fg = colours.bg_dark, bg = colours.purple })
vim.api.nvim_set_hl(0, "CmpItemKindMethod", { fg = colours.bg_dark, bg = colours.blue })
vim.api.nvim_set_hl(0, "CmpItemKindModule", { fg = colours.bg_dark, bg = colours.blue })
vim.api.nvim_set_hl(0, "CmpItemKindOperator", { fg = colours.bg_dark, bg = colours.cyan })
vim.api.nvim_set_hl(0, "CmpItemKindProperty", { fg = colours.bg_dark, bg = colours.green })
vim.api.nvim_set_hl(0, "CmpItemKindReference", { fg = colours.bg_dark, bg = colours.green })
vim.api.nvim_set_hl(0, "CmpItemKindSnippet", { fg = colours.bg_dark, bg = colours.yellow })
vim.api.nvim_set_hl(0, "CmpItemKindStruct", { fg = colours.bg_dark, bg = colours.blue1 })
vim.api.nvim_set_hl(0, "CmpItemKindText", { fg = colours.bg_dark, bg = colours.white })
vim.api.nvim_set_hl(0, "CmpItemKindTypeParameter", { fg = colours.bg_dark, bg = colours.green })
vim.api.nvim_set_hl(0, "CmpItemKindUnit", { fg = colours.bg_dark, bg = colours.red })
vim.api.nvim_set_hl(0, "CmpItemKindValue", { fg = colours.bg_dark, bg = colours.orange })
vim.api.nvim_set_hl(0, "CmpItemKindVariable", { fg = colours.bg_dark, bg = colours.green })
--
vim.api.nvim_set_hl(0, "CmpItemAbbrDeprecated", { fg = colours.comment, bg = "NONE", strikethrough = true })
vim.api.nvim_set_hl(0, "CmpItemAbbrMatch", { fg = colours.blue, bg = "NONE", bold = true })
vim.api.nvim_set_hl(0, "CmpItemAbbrMatchFuzzy", { fg = colours.blue, bg = "NONE", bold = true })

vim.api.nvim_set_hl(0, "CmpItemMenu", { fg = colours.comment, bg = "NONE", italic = true })
