vim.api.nvim_set_hl(0, "SpectreOld", {
    fg = "#db4b4b",
    bg = "#362c3d",
})
vim.api.nvim_set_hl(0, "SpectreNew", {
    fg = "#1abc9c",
    bg = "#233745",
})

require("spectre").setup {
    highlight = {
        search = "SpectreOld",
        replace = "SpectreNew",
    },
}
