local cmp = require("cmp_nvim_lsp")
local lspconfig = require("lspconfig")
local mappings = require("mappings")

-- Imports
local capabilities = cmp.default_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
local _sourceText = require("efmls-configs.utils").sourceText
local _fs = require("efmls-configs.fs")

-- Setup efm linters
-- # C++
local gcc = require("efmls-configs.linters.gcc")

-- # JSON
local jq = require("efmls-configs.linters.jq")

-- # Make
-- TODO: `checkmake` is very limited, maybe try use `unmake`?
-- local checkmake = require("efmls-configs.linters.checkmake")

-- # TOML
-- TODO: do some formatters maybe?
-- local taplo = require("efmls-configs.formatters.taplo")

-- # Languages
local languages = {
    c = { gcc },
    cpp = { gcc },
    json = { jq },
}

-- Servers
lspconfig.autotools_ls.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.bashls.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.bitbake_language_server.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
-- TODO: make a mason registry to install this
--       not sure how to do this, given that this project doesn't ship binaries
lspconfig.ccls.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.cmake.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.dockerls.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.efm.setup {
    init_options = {
        documentFormatting = true,
    },
    settings = {
        rootMarkers = { ".git/" },
        languages = languages,
    },
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.ginko_ls.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.jedi_language_server.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.jsonls.setup {
    settings = {
        json = {
            schemas = require("schemastore").json.schemas(),
            validate = { enable = true },
        },
    },
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.lemminx.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
    settings = {
        xml = {
            fileAssociations = {
                {
                    systemId = vim.env.HOME .. "/.config/opencpi/schemas/hdlworker.xsd",
                    pattern = "**-hdl.xml",
                },
                {
                    systemId = vim.env.HOME .. "/.config/opencpi/schemas/componentspec.xsd",
                    pattern = "**-comp.xml",
                },
            },
            server = {
                workDir = vim.env.HOME .. "/.cache/lemminx",
            },
        },
    },
}
lspconfig.lua_ls.setup {
    on_init = function(client)
        local path = client.workspace_folders[1].name
        if vim.fn.filereadable(path.."/.luarc.json") or vim.fn.filereadable(path.."/.luarc.jsonc") then
            return
        end
        client.config.settings.Lua = vim.tbl_deep_extend("force", client.config.settings.Lua, {
            runtime = {
                version = "LuaJIT",
            },
            workspace = {
                checkThirdParty = false,
                library = {
                    vim.env.VIMRUNTIME,
                },
            },
        })
    end,
    settings = {
        Lua = {},
    },
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.markdown_oxide.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.marksman.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
-- TODO: make a mason registry to install this
lspconfig.nixd.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.ruff.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.rust_analyzer.setup {
    settings = {
        ["rust-analyzer"] = {},
    },
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.typos_lsp.setup {
    init_options = {
        config = vim.env.HOME .. "/.typos.toml",
    },
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.vale_ls.setup {
    filetypes = { "markdown", "rst", "text" },
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.vhdl_ls.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
lspconfig.yamlls.setup {
    settings = {
        yaml = {
            schemaStore = {
                enable = false,
                url = "",
            },
            schemas = require("schemastore").yaml.schemas(),
        },
    },
    on_attach = mappings.on_attach,
    capabilities = capabilities,
}
