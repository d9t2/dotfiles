local lint = require("lint")
local _parser = require("lint.parser")

lint.linters_by_ft = {
    bash = {
        "codespell",
        "shellcheck",
    },
    c = {
        "codespell",
        "cpplint",
        "cppcheck",
        -- "gcc",  -- TODO this isn't supported
    },
    cmake = {
        "codespell",
        "cmakelint",
    },
    cpp = {
        "codespell",
        "cpplint",
        "cppcheck",
        -- "gcc",
    },
    dockerfile = {
        "codespell",
        "hadolint",
    },
    gitcommit = {
        "codespell",
        "commitlint",
    },
    json = {
        "codespell",
        -- "jq",  -- TODO this isn't supported
        "jsonlint",
    },
    lua = {
        "codespell",
        "selene",
    },
    python = {
        "codespell",
        "mypy",
    },
    make = {
        "codespell",
    },
    markdown = {
        "codespell",
        "markdownlint",
    },
    sh = {
        "codespell",
        "shellcheck",
    },
    shell = {
        "codespell",
        "shellcheck",
    },
    toml = {
        "codespell",
        -- "taplo",  -- TODO this isn't supported
    },
    vhdl = {
        "codespell",
        "vsg",
    },
    yaml = {
        -- TODO: workout how to stop this running on every single yaml file
        -- "actionlint",
        "codespell",
        "yamllint",
    },
    zsh = {
        "codespell",
        "shellcheck",
        "zsh",
    },
}

local mypy = lint.linters.mypy
mypy.args = {
    "--show-column-numbers",
    "--show-error-end",
    "--hide-error-codes",
    "--hide-error-context",
    "--no-color-output",
    "--no-error-summary",
    "--no-pretty",
    "--allow-redefinition ",
    "--strict ",
}

local selene = lint.linters.selene
selene.args = {
    "--config", vim.env.HOME .. "/.config/nvim/selene.toml",
    "--display-style", "json",
    "-"
}

local shellcheck = lint.linters.shellcheck
shellcheck.args = {
    "--format", "json",
    "--external-sources",
    "--exclude=SC1091",
    "-",
}

vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost" }, {
    callback = function()
        lint.try_lint()
    end,
})

-- vim.api.nvim_create_autocmd({ "InsertLeave", "TextChanged" }, {
--     callback = function()
--         lint.try_lint("vsg")
--     end,
-- })
