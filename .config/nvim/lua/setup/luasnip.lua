local ls = require("luasnip")

local s = ls.snippet
local t = ls.text_node
local _i = ls.insert_node
local _f = ls.function_node

ls.snippets = {
    python = {
        -- Copyright
        s("Copyright", {
            t({"# Copyright (c) " .. os.date("%Y") .. " Dominic Adam Walters"})
        }),
    },
    vhdl = {
        -- Copyright
        s("Copyright", {
            t({"-- Copyright (c) " .. os.date("%Y") .. " Dominic Adam Walters"})
        }),
        -- Basic clocked process
        s("process(clk)", {
            t({"name_proc : process(clk)", "begin", "\t"}),
            t({"if rising_edge(clk) then", "\t\t"}),
            t({"if its(reset) then", "\t\t"}),
            t({"elsif its(enable) then", "\t\t"}),
            t({"end if;", "\t"}),
            t({"end if;", ""}),
            t({"end process name_proc;"})
        }),
        -- Non clocked process
        s("process()", {
            t({"name_proc : process()", "begin"}),
            t({"end process name_proc;"})
        }),
    }
}
