-- TODO: is there a better way of doing this?
local colours = require("tokyonight.colors").setup({
    style = "storm",
})

-- Custom colours
-- # Add
-- ## Unstaged
vim.api.nvim_set_hl(
    0, "GitSignsAdd", {
        fg = colours.git.add,
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsAddNr", {
        link = "GitSignsAdd",
    }
)
-- ## Staged
vim.api.nvim_set_hl(
    0, "GitSignsStagedAdd", {
        fg = colours.comment,
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsStagedAddNr", {
        link = "GitSignsStagedAdd",
    }
)

-- # Change
-- ## Unstaged
vim.api.nvim_set_hl(
    0, "GitSignsChange", {
        fg = colours.git.change,
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsChangeNr", {
        link = "GitSignsChange",
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsChangedelete", {
        link = "GitSignsChange",
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsChangedeleteNr", {
        link = "GitSignsChange",
    }
)
-- ## Staged
vim.api.nvim_set_hl(
    0, "GitSignsStagedChange", {
        fg = colours.comment,
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsStagedChangeNr", {
        link = "GitSignsStagedChange",
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsStagedChangedelete", {
        link = "GitSignsStagedChange",
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsStagedChangedeleteNr", {
        link = "GitSignsStagedChange",
    }
)

-- # Delete
-- ## Unstaged
vim.api.nvim_set_hl(
    0, "GitSignsDelete", {
        fg = colours.git.delete,
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsDeleteNr", {
        link = "GitSignsDelete",
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsTopdelete", {
        link = "GitSignsDelete",
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsTopdeleteNr", {
        link = "GitSignsDelete",
    }
)
-- ## Staged
vim.api.nvim_set_hl(
    0, "GitSignsStagedDelete", {
        fg = colours.comment,
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsStagedDeleteNr", {
        link = "GitSignsStagedDelete",
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsStagedTopdelete", {
        link = "GitSignsStagedDelete",
    }
)
vim.api.nvim_set_hl(
    0, "GitSignsStagedTopdeleteNr", {
        link = "GitSignsStagedDelete",
    }
)

-- Setup
require("gitsigns").setup({
    signs = {
        add             = { text = "" },
        change          = { text = "󰜥" },
        delete          = { text = "" },
        topdelete       = { text = "" },
        changedelete    = { text = "󰜥" },
        untracked       = { text = "" },
    },
    signs_staged = {
        add             = { text = "" },
        change          = { text = "󰜥" },
        delete          = { text = "" },
        topdelete       = { text = "" },
        changedelete    = { text = "󰜥" },
        untracked       = { text = "" },
    },
    signs_staged_enable = true,
    signcolumn = true,
    numhl = true,
    linehl = false,
    current_line_blame = true,
})
