local alpha = require("alpha")
local dashboard = require("alpha.themes.dashboard")

-- Header
dashboard.section.header.val = {
 [[ ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗]],
 [[ ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║]],
 [[ ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║]],
 [[ ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║]],
 [[ ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║]],
 [[ ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝]],
}
dashboard.section.header.opts.hl = "Conditional"

-- Menu
dashboard.section.buttons.val = {
    dashboard.button("<leader> n f", " 󰈔 New File", ":ene <BAR> startinsert <CR>"),
    dashboard.button("<leader> f b", "  File Browser", ":Telescope file_browser <CR>"),
    dashboard.button("<leader> f p", " 󰈞 Find Project", ":Telescope project <CR>"),
    dashboard.button("<leader> f f", " 󱋡 Recent Files", ":Telescope oldfiles <CR>"),
    dashboard.button("<leader> f t", " 󰨼 Find Text", ":Telescope live_grep <CR>"),
    dashboard.button("<leader> i l", "  Install using Lazy", ":Lazy <CR>"),
    dashboard.button("<leader> i m", "  Install using Mason", ":Mason <CR>"),
    dashboard.button("<leader> u l", "  Update using Lazy", ":Lazy sync <CR>"),
    dashboard.button("<leader> u t", "  Update using Mason", ":MasonUpdateAll <CR>"),
    dashboard.button("<leader> e c", "  Edit Configuration", ":e $HOME/.config/nvim/init.lua <CR>"),
    dashboard.button("<leader>   q", " 󰗼 Quit Neovim", ":qa<CR>"),
}
dashboard.section.buttons.opts.hl = "Structure"

-- Footer
local function footer()
    local datetime = os.date(" %H:%M:%S |  %d-%m-%Y")
    local plugins_count = vim.fn.len(vim.fn.globpath("~/.local/share/nvim/lazy", "*", false, 1))
    local plugins_string = " " .. plugins_count .. " Plugins"
    local version = vim.version()
    local version_string = " v"
        .. version.major
        .. "."
        .. version.minor
        .. "."
        .. version.patch
    local quote = table.concat(require("alpha.fortune")(), "\n")
    return datetime
        .. " | " .. plugins_string
        .. " | " .. version_string
        .. "\n" .. quote
end
dashboard.section.footer.val = footer()
dashboard.section.footer.opts.hl = "Boolean"

-- Start
dashboard.opts.opts.noautocmd = true
alpha.setup(dashboard.opts)
