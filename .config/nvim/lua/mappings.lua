-- Returns
local mappings = {}

-- Generic
-- # Prevent accidental shift keying of saving and closing
vim.api.nvim_create_user_command("W", "w", {})
vim.api.nvim_create_user_command("Q", "q", {})
vim.api.nvim_create_user_command("WQ", "wq", {})

-- Open netrw in current frame
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Move visual block
vim.keymap.set("v", "J", ":m '>+1<CR><Esc>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR><Esc>gv=gv")

-- Stop viewport from moving with various commands
vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Paste without overwrite
vim.keymap.set("x", "<leader>p", "\"_dP")

-- Yank into clipboard
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")
vim.keymap.set("v", "<leader>Y", "\"+Y")

-- Discard delete
vim.keymap.set("n", "<leader>d", "\"_d")
vim.keymap.set("v", "<leader>d", "\"_d")

-- Unify Ctrl-c and ESC in Visual edit mode
vim.keymap.set("i", "<C-c>", "<Esc>")

-- Start find and replace on current word
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- Set file to be executable
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

-- # <leader><space>: Turn off search highlight
vim.api.nvim_set_keymap(
    "n",
    "<leader><Space>",
    ":nohlsearch<CR>",
    {noremap = true, silent = true}
)

-- # F5: Show all whitespace chars
vim.opt.listchars = {
    tab = ">-",
    eol = "¬",
    trail = "⌴",
    extends = ">",
    precedes = "<",
    space = "·"
}
vim.api.nvim_set_hl(
    0, "SpecialKey", {
        fg="DarkGrey"
    }
)
vim.api.nvim_set_keymap(
    "n",
    "<F5>",
    ":set list!<CR>",
    {noremap = true}
)
vim.api.nvim_set_keymap(
    "i",
    "<F5>",
    "<C-o>:set list!<CR>",
    {noremap = true}
)
vim.api.nvim_set_keymap(
    "v",
    "<F5>",
    "<C-c>:set list!<CR>",
    {noremap = true}
)

-- # F6: Terminal window
vim.opt.splitbelow = true
local shell = os.getenv("SHELL")
vim.api.nvim_set_keymap(
    "n",
    "<F6>",
    ":new term://" .. shell .. "<CR><C-w>" ..
    ":exe 'resize ' . (winheight(0) * 2/7)<CR>i",
    {noremap = true, silent = true}
)

local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>pf", builtin.find_files, {})
vim.keymap.set("n", "<C-p>", builtin.git_files, {})
vim.keymap.set("n", "<leader>ps", function()
    builtin.grep_string({ search = vim.fn.input("Grep > ") });
end)

-- Language servers
-- # Diagnostics
-- ## These relate to inline warnings and errors.
-- ## See `:help vim.diagnostic.*` for documentation.
vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float)
vim.keymap.set("n", "[d", function()
    vim.diagnostic.goto_prev({ float = false })
end)
vim.keymap.set("n", "]d", function()
    vim.diagnostic.goto_next({ float = false })
end)
vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist)

-- # on_attach
vim.api.nvim_create_autocmd("LspAttach", {
    group = vim.api.nvim_create_augroup("UserLspConfig", {}),
    callback = function(ev)
        -- ## Enable completion triggered by <c-x><c-o>
        vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"
        -- ## Buffer local mappings.
        -- ### See `:help vim.lsp.*` for documentation on any of the below
        local opts = { buffer = ev.buf }
        vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
        vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
        vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
        vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, opts)
        vim.keymap.set("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, opts)
        vim.keymap.set("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, opts)
        vim.keymap.set("n", "<leader>wl", function()
            print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end, opts)
        vim.keymap.set("n", "<leader>D", vim.lsp.buf.type_definition, opts)
        vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts)
        vim.keymap.set({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, opts)
        vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
        vim.keymap.set("n", "<leader>f", function()
            vim.lsp.buf.format { async = true }
        end, opts)
    end,
})

-- Spectre: Find and replace
vim.keymap.set("n", "<leader>S", "<cmd>lua require('spectre').toggle()<CR>", {
    desc = "Toggle Spectre"
})
vim.keymap.set("n", "<leader>sw", "<cmd>lua require('spectre').open_visual({select_word=true})<CR>", {
    desc = "Search current word"
})
vim.keymap.set("v", "<leader>sw", "<esc><cmd>lua require('spectre').open_visual()<CR>", {
    desc = "Search current word"
})
vim.keymap.set("n", "<leader>sp", "<cmd>lua require('spectre').open_file_search({select_word=true})<CR>", {
    desc = "Search on current file"
})

return mappings
