-- Get it to work in 256 color terminals
vim.opt.termguicolors = true

-- General visual configuration
-- # Change directory to files that we open
vim.opt.autochdir = true
-- # Highlight the current line
vim.opt.cursorline = true
-- # Show @@@ in the last line if it is truncated.
vim.opt.display = "truncate"
-- # Display line number
vim.opt.number = true
vim.opt.relativenumber = true
-- # Always show 5 lines around the cursor
vim.opt.scrolloff = 5
-- # Highlight matching parentheses
vim.opt.showmatch = true
-- # Stop showing mode because lualine shows it
vim.opt.showmode = false
-- # Hide command line, allowing display over statusline
vim.opt.cmdheight = 0
-- # Try to indent for me
vim.opt.smartindent = true

-- Leader
vim.g.mapleader = ","

-- Mouse
if vim.fn.has("mouse") == 1 then
    vim.opt.mouse = "a"
end

-- Tabs are 4 spaces
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 4

-- Left Margin
-- # Keep the sign column even if it isn't used
vim.opt.signcolumn = "yes"

-- Right Margin
-- # Add a right margin
vim.opt.colorcolumn = "80"
-- # Don't wrap lines
vim.opt.wrap = false

-- Swap
-- # Turn off swap files so we can open files twice
vim.opt.swapfile = false

-- Backup
vim.opt.writebackup = true
vim.opt.backupdir = vim.env.HOME .. "/.local/state/nvim/backup/"
if vim.fn.has("persistent_undo") then
    vim.opt.undofile = true
    vim.opt.undodir = vim.env.HOME .. "/.local/state/nvim/undo/"
end

-- Updates
vim.opt.updatetime = 50
