return {

    -- Package management
    {
        "williamboman/mason.nvim",
        dependencies = { "Zeioth/mason-extra-cmds", opts = {} },
        cmd = {
          "Mason",
          "MasonInstall",
          "MasonUninstall",
          "MasonUninstallAll",
          "MasonLog",
          "MasonUpdate",
          "MasonUpdateAll", -- provided by `Zeioth/mason-extra-cmds`
        },
    },
    {
        "williamboman/mason-lspconfig.nvim",
        dependencies = "williamboman/mason.nvim",
    },
    {
        "rshkarin/mason-nvim-lint",
        dependencies = {
            "williamboman/mason.nvim",
            "mfussenegger/nvim-lint",
        },
    },
    {
        "WhoIsSethDaniel/mason-tool-installer.nvim",
        dependencies = "williamboman/mason.nvim",
    },

    -- Colorscheme
    "folke/tokyonight.nvim",

    -- Minimal status line
    "nvim-lualine/lualine.nvim",

    -- Gitsigns
    "nvim-lua/plenary.nvim",
    {
        "lewis6991/gitsigns.nvim",
        dependencies = "nvim-lua/plenary.nvim",
    },

    -- Scroll bar
    "petertriho/nvim-scrollbar",

    -- Configuration for LSP and linters
    {
        "neovim/nvim-lspconfig",
        dependencies = "nvim-tree/nvim-web-devicons",
    },
    {
        "creativenull/efmls-configs-nvim",
        version = "v1.x.x",
        dependencies = "neovim/nvim-lspconfig",
    },
    "mfussenegger/nvim-lint",

    -- Startup screen
    {
        "goolord/alpha-nvim",
        dependencies = "nvim-tree/nvim-web-devicons",
    },

    -- Treesitter
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",  -- This doesn't work in same `nvim` session as the plugin was installed
    },

    -- Telescope
    -- # Core
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
            {
                "nvim-telescope/telescope-file-browser.nvim",
                dependencies = "nvim-tree/nvim-web-devicons",
            },
            {
                "nvim-telescope/telescope-fzf-native.nvim",
                build = "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && " ..
                        "cmake --build build --config Release && " ..
                        "cmake --install build --prefix build",
            },
            "nvim-telescope/telescope-project.nvim",
        },
    },

    -- Autocomplete
    "onsails/lspkind.nvim",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-buffer",
    {
        "Kamilcuk/cmp-calc",
        dependencies = { "hrsh7th/nvim-cmp" },
        init = function()
            table.insert(require("cmp").get_config().sources, { name = "calc" })
        end
    },
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-cmdline",
    "davidsierradz/cmp-conventionalcommits",
    "hrsh7th/cmp-emoji",
    "hrsh7th/nvim-cmp",
    "chrisgrieser/cmp-nerdfont",
    "hrsh7th/cmp-nvim-lua",
    "SergioRibera/cmp-dotenv",
    {
        "petertriho/cmp-git",
        dependencies = { "hrsh7th/nvim-cmp" },
        init = function()
            table.insert(require("cmp").get_config().sources, { name = "git" })
        end
    },

    -- Snippets
    {
        "L3MON4D3/LuaSnip",
        build = "make install_jsregexp",
    },
    "saadparwaiz1/cmp_luasnip",

    -- Quicker startup with caching
    "lewis6991/impatient.nvim",

    -- Markdown preview in browser
    {
        "iamcco/markdown-preview.nvim",
        cmd = {"MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop"},
        ft = {"markdown"},
        build = function() vim.fn["mkdp#util#install"]() end,
    },

    -- Sudo edit
    "lambdalisue/suda.vim",

    -- Schemas
    "b0o/schemastore.nvim",

    -- Keybind reference
    -- TODO: configure this. This will require rewriting the mappings file
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
        opts = {
            -- your configuration comes here
            -- or leave it empty to use the default settings
            -- refer to the configuration section below
        }
    },

    -- Sniprun
    -- TODO: get this added to mason.nvim?
    {
        "michaelb/sniprun",
        build = "sh install.sh",
        config = function()
            require("sniprun").setup {
                display = {
                    "Classic",
                    "VirtualTextOk",
                    "NvimNotify",
                },
            }
        end,
    },

    -- File browser with buffer editing
    {
        "stevearc/oil.nvim",
        dependencies = { "nvim-tree/nvim-web-devicons" },
    },

    -- Find and replace
    {
        "nvim-pack/nvim-spectre",
        dependencies = { "nvim-lua/plenary.nvim" },
    },

    {
        "rachartier/tiny-inline-diagnostic.nvim",
        event = "VeryLazy",
        priority = 1000, -- needs to be loaded in first
    },

    {
        "vrslev/cmp-pypi",
        dependencies = { "nvim-lua/plenary.nvim" },
        ft = "toml",
    }

}
