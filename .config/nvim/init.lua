-- Quicker loading (don't error if loading this fails)
pcall(require, "setup.impatient")

-- Load basic configuration
require("base")

-- Load package managers
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.fn.isdirectory(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup("plugins")
require("mason").setup {
    registries = {
        "file:" .. vim.env.HOME .. "/.config/nvim/mason-registry",
        "github:mason-org/mason-registry"
    },
    providers = {
        "mason.providers.client",
        "mason.providers.registry-api",
    },
}
require("mason-lspconfig").setup {
    automatic_installation = true,
}
require("mason-nvim-lint").setup {
    automatic_installation = true,
}
require("mason-tool-installer").setup {
    ensure_installed = {
        "bitbake-language-server",
        "vhdl-style-guide",
    },
}

-- Load plugin configs
-- # Appearance
-- ## Icons
require("setup.nvim-web-devicons")
-- ## Colours
require("setup.tokyonight")
-- ## Home screen
require("setup.alpha") -- requires tokyonight
-- ## Git integration on side bar
require("setup.gitsigns")
-- ## Status bar
require("setup.lualine")
-- ## Scroll bar
require("setup.scrollbar") -- requires tokyonight

-- # Completion and snips
require("setup.luasnip")
require("setup.nvim-cmp") -- requires luasnip

-- # Syntax
require("setup.language_servers") -- requires lspconfig, cmp_nvim_lsp
require("setup.nvim-lint")
require("setup.treesitter") -- requires nvim-treesitter

-- # Diagnostics
require("setup.diagnostics")

-- # UI
require("setup.telescope")

-- # Sudo edit
require("setup.suda")

-- # File browser
require("setup.oil")

-- # Spectre: Find and replace
require("setup.spectre")

-- Keybinds
require("mappings")
