# Dot files

## Installation

### Dangerous

```bash
git clone https://gitlab.com/dawalters/dotfiles.git /tmp/dotfiles
cp -rf /tmp/dotfiles/* "$HOME/."
cp -rf /tmp/dotfiles/.* "$HOME/."
```

### Less dangerous

```bash
git clone https://gitlab.com/dawalters/dotfiles.git /tmp/dotfiles
cp -rf /tmp/dotfiles/.git "$HOME/."
cd "$HOME"
git pull
```
