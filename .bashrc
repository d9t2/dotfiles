# shellcheck shell=bash

# Copyright (c) 2023 Dominic Adam Walters

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

. "$HOME/.config/shell/env.sh"
. "$HOME/.config/shell/rc.sh"

# Terminal
PS1="\[\033[00;31m\]\u\[\033[00m\]:\[\033[00;34m\]\w\[\033[00m\]\$ "
